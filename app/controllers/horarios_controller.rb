class HorariosController < ApplicationController
    before_action :set_horario, only: [:edit, :update, :destroy, :show]
    before_action :set_employees, only: [:new, :edit, :create]
    def index
        @horarios = Horario.all
    end

    def new
      @horario = Horario.new
    end

    def show 
    end

    def edit
    end

    def create
        @horario = Horario.new(horario_params)
        respond_to do |format|
          if @horario.save
            format.json {head :no_content}
            format.js
          else
            debugger
            format.json { render json: @horario.errors.full_messages, status: :unprocessable_entity }
            format.js { render :new }
          end
        end
    end

    def update
        respond_to do |format|
          if @horario.update(horario_params)
            format.json { head :no_content }
            format.js
          else
            format.json { render json: @horario.errors.full_messages, status: :unprocessable_entity }
            format.js { render :edit }
          end
        end
    end

    def destroy
        @horario.destroy
        respond_to do |format|
          format.json { head :no_content }
          format.js
        end
    end
    
    private
    def horario_params
        params.require(:horario).permit(:hora, :estado, :employee_id)
    end

    def set_horario
        @horario = Horaio.find(params[:id])
    end
    
    def set_employees
        @employees = Employee.all
    end


end