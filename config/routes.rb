Rails.application.routes.draw do
  devise_for :users
  root to: 'horarios#principal'
  resources :employees
  resources :calendar
  resources :horarios, except: [:show]
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
