class CreateHorarios < ActiveRecord::Migration[5.2]
  def change
    create_table :horarios do |t|
      t.datetime :hora
      t.string :estado
      t.references :employees, foreign_key: true

      t.timestamps
    end
  end
end
